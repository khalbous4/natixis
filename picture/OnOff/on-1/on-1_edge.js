/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
            'open-sans, sans-serif': '<script src=\"http://use.edgefonts.net/open-sans:n7,i7,n8,i8,i4,n3,i3,n4,n6,i6:all.js\"></script>'        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "horizontal",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'buttonBgOff',
                            display: 'block',
                            type: 'ellipse',
                            rect: ['102px', '6px', '42px', '40px', 'auto', 'auto'],
                            borderRadius: ["50%", "50%", "50%", "50%"],
                            fill: ["rgba(239,243,246,1)"],
                            stroke: [0,"rgb(0, 0, 0)","none"]
                        },
                        {
                            id: 'buttonBgOn',
                            display: 'none',
                            type: 'ellipse',
                            rect: ['102px', '6px', '42px', '40px', 'auto', 'auto'],
                            borderRadius: ["50%", "50%", "50%", "50%"],
                            fill: ["rgba(239,243,246,1)"],
                            stroke: [0,"rgb(0, 0, 0)","none"]
                        },
                        {
                            id: 'RectangleOff',
                            display: 'none',
                            type: 'rect',
                            rect: ['121px', '6px', '0px', '40px', 'auto', 'auto'],
                            fill: ["rgba(239,243,246,1.00)"],
                            stroke: [0,"rgb(0, 0, 0)","none"]
                        },
                        {
                            id: 'RectangleOn',
                            display: 'none',
                            type: 'rect',
                            rect: ['124px', '6px', '0px', '40px', 'auto', 'auto'],
                            fill: ["rgba(239,243,246,1.00)"],
                            stroke: [0,"rgb(0, 0, 0)","none"],
                            transform: [[],['-180']]
                        },
                        {
                            id: 'BtnOff',
                            type: 'image',
                            rect: ['108px', '12px', '30px', '28px', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"offBtn.svg",'0px','0px']
                        },
                        {
                            id: 'BtnOn',
                            display: 'none',
                            type: 'image',
                            rect: ['108px', '10px', '30px', '30px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"On.svg",'0px','0px']
                        },
                        {
                            id: 'TextOn',
                            display: 'none',
                            type: 'text',
                            rect: ['31px', '17px', '111px', '19px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 12px;\">​Connected</span></p>",
                            align: "left",
                            font: ['open-sans, sans-serif', [14, "px"], "rgba(178,199,44,1.00)", "500", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "0px", "15px", "", "none"]
                        },
                        {
                            id: 'TextOff',
                            type: 'text',
                            rect: ['146px', '16px', '111px', '19px', 'auto', 'auto'],
                            opacity: '0',
                            text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 12px;\">Disco</span><span style=\"font-size: 12px;\">nnected</span></p>",
                            align: "left",
                            font: ['open-sans, sans-serif', [18, "px"], "rgba(231,97,98,1.00)", "500", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "0px", "15px", "", "none"]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '260px', '54px', 'auto', 'auto'],
                            sizeRange: ['0px','','',''],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 2455,
                    autoPlay: true,
                    labels: {
                        "startOff": 150,
                        "startOn": 1350,
                        "endOn": 2455
                    },
                    data: [
                        [
                            "eid181",
                            "opacity",
                            500,
                            250,
                            "easeInBack",
                            "${TextOff}",
                            '0',
                            '1'
                        ],
                        [
                            "eid182",
                            "opacity",
                            1050,
                            250,
                            "easeInBack",
                            "${TextOff}",
                            '1',
                            '0'
                        ],
                        [
                            "eid98",
                            "display",
                            0,
                            0,
                            "linear",
                            "${buttonBgOn}",
                            'none',
                            'none'
                        ],
                        [
                            "eid99",
                            "display",
                            1350,
                            0,
                            "linear",
                            "${buttonBgOn}",
                            'none',
                            'block'
                        ],
                        [
                            "eid169",
                            "-webkit-transform-origin",
                            2250,
                            0,
                            "linear",
                            "${RectangleOn}",
                            [0,50],
                            [0,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid455",
                            "-moz-transform-origin",
                            2250,
                            0,
                            "linear",
                            "${RectangleOn}",
                            [0,50],
                            [0,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid456",
                            "-ms-transform-origin",
                            2250,
                            0,
                            "linear",
                            "${RectangleOn}",
                            [0,50],
                            [0,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid457",
                            "msTransformOrigin",
                            2250,
                            0,
                            "linear",
                            "${RectangleOn}",
                            [0,50],
                            [0,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid458",
                            "-o-transform-origin",
                            2250,
                            0,
                            "linear",
                            "${RectangleOn}",
                            [0,50],
                            [0,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid459",
                            "transform-origin",
                            2250,
                            0,
                            "linear",
                            "${RectangleOn}",
                            [0,50],
                            [0,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid114",
                            "opacity",
                            1605,
                            250,
                            "easeInBack",
                            "${TextOn}",
                            '0',
                            '1'
                        ],
                        [
                            "eid115",
                            "opacity",
                            2155,
                            250,
                            "easeInBack",
                            "${TextOn}",
                            '1',
                            '0'
                        ],
                        [
                            "eid167",
                            "width",
                            1355,
                            295,
                            "linear",
                            "${RectangleOn}",
                            '0px',
                            '116px'
                        ],
                        [
                            "eid168",
                            "width",
                            2155,
                            300,
                            "easeInQuint",
                            "${RectangleOn}",
                            '116px',
                            '0px'
                        ],
                        [
                            "eid96",
                            "display",
                            0,
                            0,
                            "linear",
                            "${buttonBgOff}",
                            'block',
                            'block'
                        ],
                        [
                            "eid97",
                            "display",
                            1350,
                            0,
                            "linear",
                            "${buttonBgOff}",
                            'block',
                            'none'
                        ],
                        [
                            "eid26",
                            "width",
                            250,
                            295,
                            "linear",
                            "${RectangleOff}",
                            '0px',
                            '116px'
                        ],
                        [
                            "eid35",
                            "width",
                            1050,
                            300,
                            "easeInQuint",
                            "${RectangleOff}",
                            '116px',
                            '0px'
                        ],
                        [
                            "eid353",
                            "display",
                            0,
                            0,
                            "easeInBack",
                            "${TextOn}",
                            'none',
                            'none'
                        ],
                        [
                            "eid112",
                            "display",
                            1105,
                            0,
                            "linear",
                            "${TextOn}",
                            'none',
                            'none'
                        ],
                        [
                            "eid113",
                            "display",
                            1355,
                            0,
                            "linear",
                            "${TextOn}",
                            'none',
                            'block'
                        ],
                        [
                            "eid165",
                            "display",
                            1105,
                            0,
                            "linear",
                            "${RectangleOn}",
                            'none',
                            'none'
                        ],
                        [
                            "eid166",
                            "display",
                            1355,
                            0,
                            "linear",
                            "${RectangleOn}",
                            'none',
                            'block'
                        ],
                        [
                            "eid17",
                            "display",
                            0,
                            0,
                            "linear",
                            "${RectangleOff}",
                            'none',
                            'none'
                        ],
                        [
                            "eid15",
                            "display",
                            250,
                            0,
                            "linear",
                            "${RectangleOff}",
                            'none',
                            'block'
                        ],
                        [
                            "eid95",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${BtnOff}",
                            '1',
                            '1'
                        ],
                        [
                            "eid21",
                            "opacity",
                            50,
                            155,
                            "linear",
                            "${BtnOff}",
                            '1',
                            '0.3'
                        ],
                        [
                            "eid23",
                            "opacity",
                            205,
                            175,
                            "linear",
                            "${BtnOff}",
                            '0.300000',
                            '1'
                        ],
                        [
                            "eid50",
                            "opacity",
                            1050,
                            210,
                            "linear",
                            "${BtnOff}",
                            '1',
                            '0'
                        ],
                        [
                            "eid76",
                            "top",
                            1350,
                            0,
                            "linear",
                            "${BtnOn}",
                            '10px',
                            '10px'
                        ],
                        [
                            "eid179",
                            "display",
                            0,
                            0,
                            "linear",
                            "${TextOff}",
                            'none',
                            'none'
                        ],
                        [
                            "eid180",
                            "display",
                            250,
                            0,
                            "linear",
                            "${TextOff}",
                            'none',
                            'block'
                        ],
                        [
                            "eid149",
                            "width",
                            0,
                            0,
                            "easeInBack",
                            "${Stage}",
                            '260px',
                            '260px'
                        ],
                        [
                            "eid63",
                            "background-color",
                            0,
                            0,
                            "linear",
                            "${Stage}",
                            'rgba(255,255,255,0.00)',
                            'rgba(255,255,255,0.00)'
                        ],
                        [
                            "eid45",
                            "display",
                            0,
                            0,
                            "easeInBack",
                            "${BtnOn}",
                            'none',
                            'none'
                        ],
                        [
                            "eid44",
                            "display",
                            1350,
                            0,
                            "easeInBack",
                            "${BtnOn}",
                            'none',
                            'block'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("on-1_edgeActions.js");
})("EDGE-88481312");
