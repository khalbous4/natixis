/***********************
* Actions de compositions pour Adobe Edge Animate
*
* Modifier ce fichier avec précaution, en veillant à conserver 
* les signatures et les commentaires de fonction commençant par « Edge » pour maintenir la 
* possibilité d’interagir avec ces actions depuis Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // alias pour les classes Edge couramment utilisées

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      

      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
         // insérer le code à exécuter lorsque le chargement de la composition est terminé
         sym.stop();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${buttonBgOff}", "click", function(sym, e) {
         // insérer le code du clic de souris ici
         console.log('bgRed');
         // Lire le scénario à partir d’une étiquette ou d’un moment spécifique. Par exemple :
         // sym.play(500); ou sym.play("myLabel");
         sym.play('startOff');
         
         // Accéder à une étiquette ou un moment spécifique et arrêter la lecture. Par exemple :
         // sym.stop(500); ou sym.stop("myLabel");
         sym.stop('startOn');
         
         
         

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${buttonBgOn}", "click", function(sym, e) {
         // insérer le code du clic de souris ici
         console.log('bgGreen');
         // Lire le scénario à partir d’une étiquette ou d’un moment spécifique. Par exemple :
         // sym.play(500); ou sym.play("myLabel");
         sym.play('startOff');
         
         
         // Accéder à une étiquette ou un moment spécifique et arrêter la lecture. Par exemple :
         // sym.stop(500); ou sym.stop("myLabel");
         sym.stop('startOn');
         
         

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-88481312");